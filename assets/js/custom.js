/**
 * Custom scripts for application
 * @author Vishnu <vishnuindrabalan@gmail.com>
 * @since 12-02-2014
 * @requires jQuery, bootsrap-datepicker
 */

$(function(){
	//initialize datepicker
	$('input#dob').datepicker({autoclose: true, endDate:new Date()});

	//event handler for sorting selects.
	$('.filter').change(function(){
		var qry = 'sort=' + $('select[name="sort"]').val();
		qry += '&order=' + $('select[name="order"]').val();
		var link = 'user/index?'+qry;
		reloadTable(link);
	});

	var reloadTable = function(url){
		//call list url with query & replace html of table with response html
		$.ajax({
			type:'get',
			url:url,
			success: function(html){
				$htm = $(html);
				$('table').html($($htm).find('table'));
			}
		});
	}

	//save user form submission using AJAX and formData
	$('.saveUser').submit(function(e){
		e.preventDefault();
		var fd = new FormData();  
	    $.each($(this).serializeArray(), function (i, val) {
	        fd.append(val.name, val.value);
	    });
		fd.append( 'resume', $( '#resume' )[0].files[0] );

		$.ajax({
		  url: $(this).attr('action'),
		  data: fd,
		  processData: false,
		  contentType: false,
		  type: 'POST',
		  dataType:'json',
		  success: function(data){
		    if(data.error){
		    	$('div.error').html(data.error);
		    } else {
		    	$('div.error').html('<p class="success">'+data.message+'</p>');
		    	revertForm();
		    	reloadTable(location.href);
		    }
		  },
		  error: function(){
		  		$('div.error').html('Sorry! Something went wrong!');
		  }
		});
	});

	//edit 
	$(document).on('click', '.edit', function(e){
		e.preventDefault();
		$('.btn-danger').show();
		$parent = $(this).parents('tr');
		$('h1').text('Editing user: '+$parent.find('.t_name').text());
		$('#uid').val($parent.attr('id'));
		$('#name').val($parent.find('.t_name').text());
		$('#email').val($parent.find('.t_email').text()).attr('disabled', 'disabled');
		$('input[value="' +$parent.find('.t_sex').text() +'"]').prop('checked', true);
		$('#dob').datepicker('setDate', new Date($parent.find('.t_dob').text()));
		var skills = $parent.find('.t_skills').text().split(',');
		$('.skills input[type="checkbox"]').prop('checked', false);
		$.each(skills, function(key,value){
			$('input[value="' +value+'"]').prop('checked', true);
		});
		$('.error').empty();
	});

	//delete 
	$(document).on('click', '.delete', function(e){
		e.preventDefault();
		if(confirm('Are you sure you want to delete ?')){
			var id = $(this).parents('tr').attr('id');
			$.ajax({
				type:'post',
				url:'user/delete',
				data:{id:id},
				dataType:'json',
				success: function(){
					$('div.error').html('<p class="success">User deletion succesfull!</p>');
					reloadTable(location.href);
				},
				error: function(){
					$('div.error').html('Sorry! Something went wrong!');
				}
			})
		}
	});

	//revert edit form to create form
	var revertForm =  function(){
		$('h1').text('Create User');
		$('form')[0].reset();
		$('#email').removeAttr('disabled');
		$('#uid').val('');
		$('.btn-danger').hide();
	};

	$('.btn-danger').hide();
	$('.btn-danger').click(function(e){
		e.preventDefault();
		revertForm();
	});
});