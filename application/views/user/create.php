<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Create User</title>
	<?php echo link_tag('assets/css/bootstrap.min.css');?>	
	<?php echo link_tag('assets/css/style.css');?>
	<?php echo link_tag('assets/css/datepicker.css');?>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/datepicker.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js" ></script>
</head>
<body>

	<div id="container">
		<h1>Create User</h1>

		<div id="body" class="row">
			<div class="col-md-4">
				<div class="error"></div>
				<form action="<?php echo base_url(); ?>user/save" method="post" class="saveUser" enctype="multipart/form-data">
					<div class="form-group">
						<label for="name">Name *</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="Full Name" >
					</div>
					<div class="form-group">
						<label for="email">Email *</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Email" >
					</div>
					<div class="form-group">
						<label for="password">Password *</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Password" >
					</div>
					<div class="form-group">
						<label for="dob">Date of Birth *</label>
						<input type="text" class="form-control" id="dob" name="dob" placeholder="mm/dd/yyyy">
					</div>
					<div class="form-group">
						<label for="sex">Sex *</label>
						<div class="checkbox">
							<label><input type="radio" name="sex" value="male"> Male</label>
							<label><input type="radio" name="sex" value="female"> Female</label>
						</div>
					</div>
					<div class="form-group">
						<label for="skills">Skills</label>
						<div class="checkbox skills margin-l20">
							<label><input type="checkbox" name="skills[]" value="php"> Php</label>
							<label><input type="checkbox" name="skills[]" value="mysql"> Mysql</label>
							<label><input type="checkbox" name="skills[]" value="jquery"> jQuery</label>
							<label><input type="checkbox" name="skills[]" value="javascript"> javascript</label>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail">Resume *</label>
						<input type="file" class="form-control" id="resume" name="resume">
					</div>

					<input type="hidden" id="uid" name="uid">
					<button type="button" class="btn btn-primary btn-danger">Cancel</button>
					<button type="submit" class="btn btn-primary pull-right">Save</button>
				</form>
			</div>
			<div class="col-md-8">
				SORT BY 
				<select name="sort" class="filter">
					<option value="timestamp">Creation date</option>
					<option value="skills_last_updated">Skills Updated</option>
					<option value="skill_count">Number of Skills</option>
				</select>
				<select name="order" class="filter">
					<option value="ASC">Ascending</option>
					<option value="DESC" selected>Descending</option>
				</select>
				<br><br>
				<div class="datagrid"><table>
					<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>DOB</th>
						<th>Sex</th>
						<th>Skills</th>
						<th>Resume</th>
						<th>Manage</th>
					</tr>
					</thead>
					
					<tbody>
					<?php foreach($users as $user) {?>
						<tr id="<?php echo $user->id ?>">
							<td class="t_name"><?php echo $user->name; ?></td>
							<td class="t_email"><?php echo $user->email; ?></td>
							<td class="t_dob"><?php echo date('d-M-Y', strtotime($user->dob)); ?></td>
							<td class="t_sex"><?php echo $user->sex; ?></td>
							<td class="t_skills"><?php echo $user->skills; ?></td>
							<td><?php if($user->resume) { ?><a target="_blank" href="<?php echo base_url(); echo $user->resume; ?>">Download</a><?php } ?></td>
							<td><a href="#" class="edit">Edit</a> &nbsp <a class="delete" href="#">Delete</a></td>
						</tr>
					<?php } ?>
					<?php if(!$users) { ?>
						<tr><td colspan="7" class="text-center">No users added yet!</td></tr>
					<?php } ?>
					</tbody>
				</table></div>
			</div>

		</div>

		<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
	</div>
</body>
</html>