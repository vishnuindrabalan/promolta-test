<?php 

class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
	 * Save user
     */
    public function save($user, $skills){
        //handle edit
        if(isset($user['id'])){
            $uid = $user['id'];
            unset($user['id']);
            $this->db->where('id',$uid);
            $this->db->update('users', $user);
        } else {
            $this->db->insert('users', $user);
            $uid = $this->db->insert_id();
        }
    	
        //clear current skills
        $this->db->where('user_id', $uid);
        $this->db->delete('skills');
        //insert fresh list
    	foreach ($skills as $key => $skill) {
    		$this->db->insert('skills', array('user_id'=>$uid, 'skill'=>$skill, 'updated_at'=>date('Y-m-d H:i:s')));
    	}
    	return true;
    }

    public function list_users($sort,$order){
    	$this->db->select("users.*, COUNT(skills.skill) as skill_count, GROUP_CONCAT(skills.skill) as skills, MAX(skills.updated_at) as skills_last_updated");
	 	$this->db->join("skills", "users.id=skills.user_id", "left");
	 	$this->db->group_by("users.id");
	 	if($sort){
	 		$this->db->order_by($sort,$order);
	 	} else {
            $this->db->order_by('timestamp','DESC');            
        }
	 	$query = $this->db->get("users");
    	return $query->result();
    }

    /**
     * Delete
     */
    public function remove($id){
        $this->db->where('id', $id);
        $this->db->delete('users');
        return true;
    }
}