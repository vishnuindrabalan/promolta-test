<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
    	$this->load->model('User_model');
    	$this->load->library('form_validation');
	}

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		$sort = $this->input->get('sort');
		$order = $this->input->get('order');
		$users = $this->User_model->list_users($sort, $order);
		$this->load->view('user/create', array('users' => $users));
	}

	public function save()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('sex', 'Sex', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required');
		//omit email & pass validation in case of edit
		if(!$this->input->post('uid') || $this->input->post('uid') == '') {
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		}

		if ($this->form_validation->run() == FALSE) {
			 $response = array('error'=>validation_errors(), 'message'=>"Error!");
		} else {
			$user = array(
				'name'	=>	$this->input->post('name'),
				'email'	=>	$this->input->post('email'),
				'password'	=>	md5($this->input->post('password')),
				'sex'	=>	$this->input->post('sex'),
				'dob'	=>	date("Y-m-d H:i:s",strtotime($this->input->post('dob'))), //convert to mysql timestamp
			);

			//handle edit
			if($this->input->post('uid') && $this->input->post('uid') != ''){
				$user['id'] = $this->input->post('uid');
				unset($user['email']);
			}

			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'doc|docx|pdf';
			$config['max_size']	= '1000';
			$this->load->library('upload', $config);

			if (isset($_FILES['resume']) && !$this->upload->do_upload('resume')) {
				$response = array('error'=>$this->upload->display_errors(), 'message'=>"File error!");
			} else {
				if(isset($_FILES['resume'])){					
					$user['resume'] = 'uploads/'.$_FILES['resume']['name'];
				}
				$skills = $this->input->post('skills');
				$this->User_model->save($user,$skills);
				$response = array('error'=>false, 'message'=>"Saved succesfully!");
			}
		}
		echo json_encode($response);	
	}

	public function delete()
	{
		if($this->input->post('id') && $this->input->post('id') != ''){
			$this->User_model->remove($this->input->post('id'));
			echo json_encode(array('success'=>true));
		}

	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */